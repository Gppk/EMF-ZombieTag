#!/bin/bash

apt-get update
apt-get install bzip2
cd ~

# Install arduino-cli
apt-get install curl -y
curl -L -o arduino-cli.tar.bz2 https://downloads.arduino.cc/arduino-cli/arduino-cli-latest-linux64.tar.bz2
tar xjf arduino-cli.tar.bz2
rm arduino-cli.tar.bz2
mv arduino-cli /usr/bin/arduino-cli

# Install python, pip and pyserial
apt install python-is-python3 -y
apt install python3-pip -y
pip3 install pyserial

# Install esp32 core
printf "board_manager:\n  additional_urls:\n    - https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json\n" > .arduino-cli.yaml
arduino-cli core update-index --config-file .arduino-cli.yaml
arduino-cli core install esp32:esp32 --config-file .arduino-cli.yaml


# Install 'third-party' packages: find proper location and 'git clone'
apt-get install git -y
mkdir -p /root/Arduino/libraries
cd /root/Arduino/libraries

pwd
ls -alh .
git clone https://github.com/ThingPulse/esp8266-oled-ssd1306.git
git clone https://github.com/arduino-libraries/ArduinoBLE.git
git clone https://github.com/adafruit/Adafruit_BusIO.git
git clone https://github.com/adafruit/Adafruit-GFX-Library.git
git clone https://github.com/adafruit/Adafruit_SSD1306.git
git clone https://github.com/bblanchon/ArduinoJson.git

cd ~