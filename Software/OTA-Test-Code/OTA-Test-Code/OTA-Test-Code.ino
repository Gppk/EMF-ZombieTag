/* WebOTA.ino
 *  
 * by Roland Pelayo 
 * 
 * Update ESP32 firmware via external web server
 */
 
#include <WiFi.h>
#include <HTTPClient.h>
#include <Update.h>

#include <ArduinoJson.h>
#include <string.h>
// Your WiFi credentials. Ideally stored in an arduino_secrets.h
// const char* ssid = "";
// const char* password = "";
// File with SSID & Passwords not stored in git
#include "arduino_secrets.h"

// Global variables
int totalLength;       //total size of firmware
int currentLength = 0; //current size of written firmware

// Get this in your project settings on github
static String projectID = "45980715";

HTTPClient client;

void setup() {
  Serial.begin(115200);
  
  beginWifi();

  String mostRecentReleaseUrl = getMostRecentRelease();
  //getFirmwareFile();
}

void loop() {}

void beginWifi() {
  // Start WiFi connection
  WiFi.mode(WIFI_MODE_STA);        
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");  
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Wasn't entirely sure how to do string parsing with String so this function uses std::string
String getReleaseId(std::string recentReleaseQueryResponse){

  std::string delimiter = "/";
  std::string token;

  size_t pos = 0;
  while ((pos = recentReleaseQueryResponse.find(delimiter)) != std::string::npos) {
      token = recentReleaseQueryResponse.substr(0, pos);
      Serial.println(token.c_str());
      recentReleaseQueryResponse.erase(0, pos + delimiter.length());
  }
  // Remove the last two charachters a . and an "
  recentReleaseQueryResponse.pop_back();recentReleaseQueryResponse.pop_back();
  Serial.println(recentReleaseQueryResponse.c_str());
  
  return recentReleaseQueryResponse.c_str();
}

String getMostRecentRelease(){
    
    String mostRecentReleaseQueryUrl = "https://gitlab.com/api/v4/projects/" + projectID + "/releases/permalink/latest";
      
    String recentReleaseQueryResponse = sendGetMessage(mostRecentReleaseQueryUrl);
    String commitID = getReleaseId(recentReleaseQueryResponse.c_str());

    
    String mostRecentReleaseUrlBegin = "https://gitlab.com/api/v4/projects/" + projectID;
    String mostRecentReleaseUrlEnd = "/releases/" + commitID;
    String mostRecentRelaseUrlFull = mostRecentReleaseUrlBegin + mostRecentReleaseUrlEnd;

    String releaseJSONstr = sendGetMessage(mostRecentRelaseUrlFull);

    StaticJsonDocument<0> filter;
    filter.set(true);
    DynamicJsonDocument doc(6144);
    DeserializationError error = deserializeJson(doc, releaseJSONstr, DeserializationOption::Filter(filter));

    /* BEGIN PARSE FOR ASSET LINK */
    JsonObject assets = doc["assets"];
    int assets_count = assets["count"]; // 5

    for (JsonObject assets_source : assets["sources"].as<JsonArray>()) {
      const char* assets_source_format = assets_source["format"]; // "zip", "tar.gz", "tar.bz2", "tar"
      const char* assets_source_url = assets_source["url"];
    }

    JsonObject assets_links_0 = assets["links"][0];
    long assets_links_0_id = assets_links_0["id"]; // 1664502
    const char* assets_links_0_name = assets_links_0["name"]; // "Human-Zombie-Device.bin"
    const char* assets_links_0_url = assets_links_0["url"];
    const char* assets_links_0_direct_asset_url = assets_links_0["direct_asset_url"];
    const char* assets_links_0_link_type = assets_links_0["link_type"]; // "other"
    /* END PARSE FOR ASSET LINK */

    String fullBinPath = (String)assets_links_0_direct_asset_url + "/" +  (String)assets_links_0_name;
    Serial.print("Direct Asset Url: ");
    Serial.println(fullBinPath);
    
    return fullBinPath;
}

// Send a GET request to a URL, Return Payload
String sendGetMessage(String url) {
      
      HTTPClient http;
      String payload = "";
      // Your Domain name with URL path or IP address with path
      http.begin(url.c_str());

      // Send HTTP GET request
      int httpResponseCode = http.GET();
      
      if (httpResponseCode>0) {
        Serial.print("HTTP Response code: ");
        Serial.println(httpResponseCode);
        payload = http.getString();
        Serial.println(payload);
      }
      else {
        Serial.print("Error code: ");
        Serial.println(httpResponseCode);
      }
      // Free resources
      http.end();

      return payload;
}

void getFirmwareFile(String binFileUrl) {

  // Connect to external web server
  client.begin(binFileUrl);
  // Get file, just to check if each reachable
  int resp = client.GET();
  Serial.print("Response: ");
  Serial.println(resp);
  // If file is reachable, start downloading
  if(resp == 200){
      // get length of document (is -1 when Server sends no Content-Length header)
      totalLength = client.getSize();
      // transfer to local variable
      int len = totalLength;
      // this is required to start firmware update process
      Update.begin(UPDATE_SIZE_UNKNOWN);
      Serial.printf("FW Size: %u\n",totalLength);
      // create buffer for read
      uint8_t buff[128] = { 0 };
      // get tcp stream
      WiFiClient * stream = client.getStreamPtr();
      // read all data from server
      Serial.println("Updating firmware...");
      while(client.connected() && (len > 0 || len == -1)) {
           // get available data size
           size_t size = stream->available();
           if(size) {
              // read up to 128 byte
              int c = stream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
              // pass to function
              updateFirmware(buff, c);
              if(len > 0) {
                 len -= c;
              }
           }
           delay(1);
      }
  }else{
    Serial.println("Cannot download firmware file. Only HTTP response 200: OK is supported. Double check firmware location #defined in HOST.");
  }
  client.end();
}

// Function to update firmware incrementally
// Buffer is declared to be 128 so chunks of 128 bytes
// from firmware is written to device until server closes
void updateFirmware(uint8_t *data, size_t len){
  Update.write(data, len);
  currentLength += len;
  // Print dots while waiting for update to finish
  Serial.print('.');
  // if current length of written firmware is not equal to total firmware size, repeat
  if(currentLength != totalLength) {
      Serial.println("Not got the whole thing yet....");
      return;
  }
  Serial.println("Got the whole thing?");

  Update.end(true);
  Serial.printf("\nUpdate Success, Total Size: %u\nRebooting...\n", currentLength);
  // Restart ESP32 to see changes 
  ESP.restart();
}
