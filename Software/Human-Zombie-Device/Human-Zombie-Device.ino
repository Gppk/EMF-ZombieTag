// add in this
// https://hackmd.io/@exradr/clang-format

// code for DOITESP32 DEVKIT V1

// Custom ZTag Libraries
#include "DeviceState.h"
#include "EEPROM_Manager.h"
#include "ArduinoBle_Manager.h"
#include "Oled_Manager.h"
#include "SerialHelper.h"


#include <string>

// OTA Lib
#include <WiFi.h>
#include "WebHelperFunctions.h" 
#include "FirmwareUpdateHelpers.h"
#include "arduino_secrets.h"

// Should align to the version file used in the pipeline.
const int version = 3;
// gitlab project ID
const String projectID = "45980715";

ArduinoBle_Manager bleManager;

void setup() {
    // initialize Serial Monitor
    Serial.begin(115200);
    init_oled();

    write_oled_line(1, "ZTAG DEVICE");
    Serial.println("ZTAG DEVICE");
    Serial << "Current Version: " << version << '\n';

    // This function will either return and we'll continue or we will reflash.
    checkIfUpdateRequiredUpdateIfSo();
  


    bleManager.initBleZTagDevice();
    Serial.println("Ble Initializing");
     

    // Get the saved state from EEPROM and output it
    deviceState = init_and_read_device_state();
    numberOfInfectionTicks = 0;   // set the infection counter to zero.
    write_oled_and_serial_line(2, DeviceStateToString(deviceState));

    bleManager.updateZtagBeaconName(DeviceStateToString(DeviceState::human));
    
}

void loop() {

    bleManager.pollBle(); 
    
    if (bleManager.getFoundZtagDevices().size() > 0) {
        // If we find a zTag device then store that in foundZTagData        
        Serial << "number of ZTags found: " << bleManager.getFoundZtagDevices().size() << '\n';
    }

    // Write some behaviour code, somewhere that isn't loop!

    // Then write some OLED Stats Update Code, probably somewhere that isn't loop!    
}

void checkIfUpdateRequiredUpdateIfSo() {
    
    beginWifi();   
    String releaseName = "";
    String mostRecentReleaseUrl = getMostRecentReleaseFromGitlab(projectID, releaseName);
    Serial << "releaseName: " <<  releaseName << '\n';

    std::string str = releaseName.c_str();
    size_t last_index = str.find_last_not_of("0123456789");
    std::string result = str.substr(last_index + 1);
    int gitlabVersion = atoi( result.c_str() );

    Serial.printf("const device version: : %u\n",version);
    Serial.printf("parsed releaseName int: : %u\n",gitlabVersion);

    if (version < gitlabVersion) {
        Serial.println("Updating as device version is out of date.");
        getFirmwareFileAndUpdate(mostRecentReleaseUrl);
    }
    else {
        Serial.println("Current version is equal or greater than the gitlab release. No update required");
    }

    endWifi();
}

// Start up and connect to an access point.
void beginWifi() {
  // Start WiFi connection
  WiFi.mode(WIFI_MODE_STA);        
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");  
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void endWifi() {
    WiFi.disconnect(true);
    WiFi.mode(WIFI_OFF);
}