#include "WebHelperFunctions.h"

#include <WiFi.h>
#include <HTTPClient.h>
#include <Update.h>

#include <ArduinoJson.h>
#include <string.h>

// Wasn't entirely sure how to do string parsing with String so this function uses std::string 
// but returns an arduino String for compatibility with the rest of the code
String getReleaseId(String recentReleaseQueryResponseStr) {
  std::string delimiter = "/";
  std::string token;
  std::string recentReleaseQueryResponse = recentReleaseQueryResponseStr.c_str();
  size_t pos = 0;
  while ((pos = recentReleaseQueryResponse.find(delimiter)) != std::string::npos) {
      token = recentReleaseQueryResponse.substr(0, pos);
      Serial.println(token.c_str());
      recentReleaseQueryResponse.erase(0, pos + delimiter.length());
  }
  // Remove the last two charachters a . and an "
  // TODO: Add some error handling in case its empty for some reason.
  recentReleaseQueryResponse.pop_back();recentReleaseQueryResponse.pop_back();
  Serial.println(recentReleaseQueryResponse.c_str());
  
  return recentReleaseQueryResponse.c_str();
}

String getMostRecentReleaseFromGitlab(const String projectID, String &releaseName ){
    
    String mostRecentReleaseQueryUrl = "https://gitlab.com/api/v4/projects/" + projectID + "/releases/permalink/latest";
      
    String recentReleaseQueryResponse = sendGetMessage(mostRecentReleaseQueryUrl);
    String commitID = getReleaseId(recentReleaseQueryResponse);

    
    String mostRecentReleaseUrlBegin = "https://gitlab.com/api/v4/projects/" + projectID;
    String mostRecentReleaseUrlEnd = "/releases/" + commitID;
    String mostRecentRelaseUrlFull = mostRecentReleaseUrlBegin + mostRecentReleaseUrlEnd;

    String releaseJSONstr = sendGetMessage(mostRecentRelaseUrlFull);

    StaticJsonDocument<0> filter;
    filter.set(true);
    DynamicJsonDocument doc(6144);
    DeserializationError error = deserializeJson(doc, releaseJSONstr, DeserializationOption::Filter(filter));

    /* BEGIN PARSE FOR ASSET LINK */

    const char* name = doc["name"]; // "Release Bin File <version>"
    releaseName = name;
    JsonObject assets = doc["assets"];
    int assets_count = assets["count"]; // 5

    for (JsonObject assets_source : assets["sources"].as<JsonArray>()) {
      const char* assets_source_format = assets_source["format"]; // "zip", "tar.gz", "tar.bz2", "tar"
      const char* assets_source_url = assets_source["url"];
    }

    JsonObject assets_links_0 = assets["links"][0];
    long assets_links_0_id = assets_links_0["id"]; // 1664502
    const char* assets_links_0_name = assets_links_0["name"]; // "name.bin"
    const char* assets_links_0_url = assets_links_0["url"];
    const char* assets_links_0_direct_asset_url = assets_links_0["direct_asset_url"];
    const char* assets_links_0_link_type = assets_links_0["link_type"]; // "other"
    /* END PARSE FOR ASSET LINK */

    String fullBinPath = "";
    fullBinPath = (String)assets_links_0_direct_asset_url + "/" +  (String)assets_links_0_name;
    Serial.print("Direct Asset Url: ");
    Serial.println(fullBinPath);
    
    return fullBinPath;
}

// Send a GET request to a URL, Return Payload. not specific to gitlab
String sendGetMessage(String url) {
          
    HTTPClient http;
    String payload = "";
    // Your Domain name with URL path or IP address with path
    http.begin(url.c_str());

    // Send HTTP GET request
    int httpResponseCode = http.GET();
    
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      payload = http.getString();
      Serial.println(payload);
    }
    else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }
    // Free resources
    http.end();
    return payload;
}
