#include "FirmwareUpdateHelpers.h"

#include <HTTPClient.h>
#include <Update.h>

#include <ArduinoJson.h>
#include <string.h>

// Global variables
HTTPClient httpClient;
int totalLength;       //total size of firmware
int currentLength = 0; // Global variable because of recursive function

// If this function returns at all, it didn't work.
// If this function succeeds your ESP will update
void getFirmwareFileAndUpdate(String firmwareFileHttpUrl) {

  // Connect to external web server
  httpClient.begin(firmwareFileHttpUrl);
  int httpResponse = httpClient.GET();
  Serial.println("Response: " + httpResponse);

  // If we get the file continue, otherwise just return from the function and don't update
  if(httpResponse == 200) {
      
      /// Store this length for useage in the flash function
      totalLength = httpClient.getSize();
      int len = totalLength;
      
      // Kick off an ESP update
      Update.begin(UPDATE_SIZE_UNKNOWN);
      Serial.printf("FW Size: %u\n",totalLength);

      // create buffer for reading in the bin file response
      uint8_t buff[128] = { 0 };
      // Get a tcp stream
      WiFiClient * tcpSstream = httpClient.getStreamPtr();

      // read all data from server
      Serial.println("Downloading firmware and sending to Update class...");
      while(httpClient.connected() && (len > 0 || len == -1)) {
           // get available data size
           size_t size = tcpSstream->available();
           if(size) {
              // read up to 128 bytes
              int i = tcpSstream->readBytes(buff, ((size > sizeof(buff)) ? sizeof(buff) : size));
              // pass to function
              AddFirmwareUpdateIncrement(buff, i);
              if(len > 0) {
                 len -= i;
              }
           }
           delay(1);
      }
  }else{
    Serial.println("Cannot download firmware file. Only HTTP response 200: OK is supported. Double check firmware location #defined in HOST.");
  }
  // We no longer need the httpClient so close it.
  httpClient.end();
}

// Function to update firmware incrementally
// firmware is written to the device until server closes
void AddFirmwareUpdateIncrement(uint8_t *data, size_t len) {  
  
  // Write the new data to the Update class
  Update.write(data, len);
  currentLength += len;

  // Print dots while waiting for update to finish
  Serial.print('.');
  
  // if current length of the firmware is not equal to total firmware size, return back to the getFirmwareFileAndUpdate function
  if(currentLength != totalLength) {
      return;
  }

  // We have all the file, finish the update
  Update.end(true);
  Serial.printf("\nUpdate Success, Total Size: %u\nRebooting...\n", currentLength);

  // Restart ESP32 which boots the new code
  ESP.restart();
}
